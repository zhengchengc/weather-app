import React from 'react'

const Footer = (props) => {
  return (
      <footer className="weather-channel__footer">
        <p>Powered by Node and React</p>
      </footer>
  )
}

export default Footer
